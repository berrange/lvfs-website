#!/usr/bin/python3
#
# Copyright 2017 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later

import json
from typing import Any

import humanize

from flask import Blueprint, render_template, make_response, flash, redirect, url_for, g
from flask_login import login_required
from sqlalchemy.exc import NoResultFound

from lvfs import db

from lvfs.util import admin_login_required
from lvfs.vendors.models import Vendor
from lvfs.tasks.models import Task

from .models import Remote

bp_metadata = Blueprint("metadata", __name__, template_folder="templates")


@bp_metadata.route("/<group_id>")
@login_required
def route_remote(group_id: str) -> Any:
    """
    Generate a remote file for a given QA group.
    """

    # find the vendor
    try:
        vendor = db.session.query(Vendor).filter(Vendor.group_id == group_id).one()
    except NoResultFound:
        flash("No vendor with that name", "danger")
        return redirect(url_for("metadata.route_view"))

    # security check
    if not vendor.check_acl("@view-metadata"):
        flash("Permission denied: Unable to view metadata", "danger")
        return redirect(url_for("metadata.route_view"))

    # generate file
    remote: list[str] = []
    remote.append("[fwupd Remote]")
    remote.append("Enabled=true")
    remote.append("Title=Embargoed for " + group_id)
    remote.append("RefreshInterval=3600")
    # change this to xz when we hard depend on fwupd >= 1.8.8
    remote.append(
        f"MetadataURI=https://fwupd.org/downloads/{vendor.remote.get_basename_newest('gz')}"
    )
    remote.append("ReportURI=https://fwupd.org/lvfs/firmware/report")
    remote.append("OrderBefore=lvfs,fwupd")
    remote.append("# ↓ MODIFY ↓ -- the email address you use to login to the LVFS")
    remote.append("Username=")
    remote.append("# ↓ MODIFY ↓ -- a *USER TOKEN* from https://fwupd.org/lvfs/profile")
    remote.append("Password=")

    fn = group_id + "-embargo.conf"
    response = make_response("\n".join(remote))
    response.headers["Content-Disposition"] = "attachment; filename=" + fn
    response.mimetype = "text/plain"
    return response


@bp_metadata.route("/")
@login_required
def route_view() -> Any:
    """
    Show all metadata available to this user.
    """

    # show all embargo metadata URLs when admin user
    vendors: list[Vendor] = []
    for vendor in db.session.query(Vendor):
        if vendor.is_account_holder and vendor.check_acl("@view-metadata"):
            vendors.append(vendor)
    remotes: dict[str, Remote] = {}
    for r in db.session.query(Remote):
        remotes[r.name] = r
    return render_template(
        "metadata.html", category="firmware", vendors=vendors, remotes=remotes
    )


@bp_metadata.post("/rebuild")
@login_required
@admin_login_required
def route_rebuild() -> Any:
    """
    Forces a rebuild of all metadata.
    """

    # update metadata
    scheduled_signing = None
    for r in db.session.query(Remote):
        r.is_valid = False
        if r.is_public:
            scheduled_signing = r.scheduled_signing
    db.session.commit()
    if scheduled_signing:
        flash(
            f"Metadata will be rebuilt {humanize.naturaltime(scheduled_signing)}",
            "info",
        )
    return redirect(url_for("metadata.route_view"))


@bp_metadata.post("/rebuild/<int:remote_id>")
@login_required
@admin_login_required
def route_rebuild_remote(remote_id: int) -> Any:
    """
    Forces a rebuild of one metadata remote.
    """

    # update metadata
    try:
        r = db.session.query(Remote).filter(Remote.remote_id == remote_id).one()
    except NoResultFound:
        flash("No remote with that ID", "danger")
        return redirect(url_for("metadata.route_view"))

    # asynchronously rebuilt
    flash(f"Remote {r.name} is being regenerated", "info")
    db.session.add(
        Task(
            value=json.dumps({"id": r.remote_id}),
            caller=__name__,
            user=g.user,
            function="lvfs.metadata.utils.task_regenerate_remote",
        )
    )
    r.is_valid = False
    db.session.commit()

    # success
    return redirect(url_for("metadata.route_view"))
