#!/usr/bin/python3
#
# Copyright 2023 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later
#
# pylint: disable=protected-access,wrong-import-position,too-many-public-methods

import os
import sys
import unittest

# allows us to run this from the project root
sys.path.append(os.path.realpath("."))

from lvfs.metadata.manifest import Manifest


class TestManifest(unittest.TestCase):
    def test_export(self):
        manifest = Manifest()
        self.assertEqual(manifest.items, [])
        manifest.add_file("foo.bin", "123456", 9876)
        manifest.add_file("foo.bin", "123456", 9876)
        self.assertEqual(len(manifest.items), 1)
        self.assertEqual(manifest.export(), "foo.bin,123456,9876\n")

    def test_import(self):
        manifest = Manifest(buf="foo.bin,123456,9876\n")
        self.assertEqual(len(manifest.items), 1)
        item = manifest.get_by_filename_fnmatch("*.NOTGOINGTOEXIST")
        self.assertEqual(item, [])

        items = manifest.get_by_filename_fnmatch("*.bin")
        self.assertEqual(len(items), 1)
        self.assertEqual(items[0].filename, "foo.bin")
        self.assertEqual(items[0].checksum, "123456")
        self.assertEqual(items[0].size, 9876)


if __name__ == "__main__":
    unittest.main()
