#!/usr/bin/python3
#
# Copyright 2018 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later

import json
from typing import Any, Optional

from flask import (
    Blueprint,
    Response,
    flash,
    redirect,
    render_template,
    request,
    url_for,
    g,
)
from flask_login import login_required
from sqlalchemy.orm import Query
from sqlalchemy.exc import NoResultFound


from lvfs import db, auth

from lvfs.users.models import User, UserCertificate
from lvfs.tasks.models import Task
from lvfs.util import (
    admin_login_required,
    _json_error,
    _json_success,
    _pkcs7_signature_info,
    _pkcs7_signature_verify,
)

from .models import Report, ReportAttribute
from .utils import process_report_data

bp_reports = Blueprint("reports", __name__, template_folder="templates")


@bp_reports.route("/<report_id>")
@login_required
def route_view(report_id: int) -> Any:
    try:
        report = db.session.query(Report).filter(Report.report_id == report_id).one()
    except NoResultFound:
        return _json_error("Report does not exist")
    # security check
    if not report.check_acl("@view"):
        return _json_error("Permission denied: Unable to view report")
    response = json.dumps(report.to_dict(), indent=4, separators=(",", ": "))
    return Response(response=response, status=400, mimetype="application/json")


@bp_reports.route("/<int:report_id>/share/<guid>")
@auth.login_required
def route_share(report_id: int, guid: str) -> Any:
    try:
        report = db.session.query(Report).filter(Report.report_id == report_id).one()
    except NoResultFound:
        return _json_error("Report does not exist")
    attr = report.get_attribute_by_key("Guid")
    if not attr or attr.value != guid:
        return _json_error("GUID does not match")
    response = json.dumps(report.to_dict(), indent=4, separators=(",", ": "))
    return Response(response=response, status=200, mimetype="application/json")


@bp_reports.route("/<report_id>/details")
@login_required
def route_show(report_id: int) -> Any:
    try:
        report = db.session.query(Report).filter(Report.report_id == report_id).one()
    except NoResultFound:
        flash("Report does not exist", "danger")
        return redirect(url_for("main.route_dashboard"))
    # security check
    if not report.check_acl("@view"):
        flash("Permission denied: Unable to view report", "danger")
        return redirect(url_for("main.route_dashboard"))
    return render_template("report-details.html", rpt=report)


@bp_reports.post("/<report_id>/delete")
@login_required
def route_delete(report_id: int) -> Any:
    try:
        report = (
            db.session.query(Report)
            .filter(Report.report_id == report_id)
            .with_for_update(of=Report)
            .one()
        )
    except NoResultFound:
        flash("No report found!", "danger")
        return redirect(url_for("analytics.route_reports"))
    # security check
    if not report.check_acl("@delete"):
        flash("Permission denied: Unable to delete report", "danger")
        return redirect(url_for("reports.route_show", report_id=report_id))

    # update the counts
    db.session.add(
        Task(
            value=json.dumps({"id": report.firmware_id}),
            caller=__name__,
            user=g.user,
            function="lvfs.firmware.utils.task_modify_fw_report_cnts",
        )
    )

    # delete
    for e in report.attributes:
        db.session.delete(e)
    db.session.delete(report)
    db.session.commit()

    flash("Deleted report", "info")
    return redirect(url_for("analytics.route_reports"))


def route_report(user: Optional[User] = None) -> Any:
    """Upload a report"""

    msgs: list[str] = []

    # only accept form data
    if request.method != "POST":
        return _json_error("only POST supported")

    # parse both content types, either application/json or multipart/form-data
    signature = None
    if request.data:
        payload = request.data.decode("utf8")
    elif request.form:
        data = request.form.to_dict()
        if "payload" not in data:
            return _json_error("No payload in multipart/form-data")
        payload = data["payload"]
        if "signature" in data:
            signature = data["signature"]
    else:
        return _json_error("No data")

    # find user and verify
    if not user and signature:
        try:
            info = _pkcs7_signature_info(signature)
        except IOError as e:
            return _json_error(f"Signature invalid: {e!s}")
        if "serial" not in info:
            return _json_error("Signature invalid, no signature")
        try:
            crt = (
                db.session.query(UserCertificate)
                .filter(UserCertificate.serial == info["serial"])
                .one()
            )
        except NoResultFound:
            return _json_error(
                f"Certificate serial {info['serial']} did not match user"
            )
        try:
            _pkcs7_signature_verify(crt.text, payload, signature)
        except IOError:
            return _json_error("Signature did not validate")
        msgs.append(f"Certificate serial matched {crt.user.display_name}")
        user = crt.user

    # add report
    try:
        msgs2, uris = process_report_data(payload, user)
        msgs.extend(msgs2)
    except ValueError as e:
        return _json_error(str(e))

    # put messages and URIs on one line
    return _json_success(
        msg="; ".join(msgs) if msgs else None,
        uri=" ".join(list(set(uris))) if uris else None,
    )


@bp_reports.post("/export")
@login_required
@admin_login_required
def route_export() -> Any:
    data = request.form.to_dict()
    subqs: list[Query] = []
    if "attrs" not in data or not data["attrs"]:
        flash("No attributes specified", "info")
        return redirect(url_for("analytics.route_reportattrs"))
    for attr in data["attrs"].split(","):
        kvs = attr.split("=", maxsplit=1)
        if len(kvs) == 1:
            subqs.append(
                db.session.query(ReportAttribute.report_id)
                .filter(ReportAttribute.key == kvs[0])
                .subquery()
            )
        else:
            subqs.append(
                db.session.query(ReportAttribute.report_id)
                .filter(ReportAttribute.key == kvs[0], ReportAttribute.value == kvs[1])
                .subquery()
            )
    stmt = db.session.query(Report)
    for subq in subqs:
        stmt = stmt.join(subq, Report.report_id == subq.c.report_id)
    res = []
    for r in stmt.order_by(Report.report_id.desc()):
        report_res = {"report_id": r.report_id}
        for attr in data["attrs"].split(","):
            attr_kv = r.get_attribute_by_key(attr.split("=")[0])
            report_res[attr_kv.key] = attr_kv.value
        res.append(report_res)
    response = json.dumps(res, indent=4, separators=(",", ": "))
    return Response(response=response, status=400, mimetype="application/json")
