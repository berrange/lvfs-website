#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright 2019 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later
#
# pylint: disable=unused-argument

from collections import Counter
from typing import Dict

from lvfs import db

from lvfs.components.models import ComponentShard, ComponentShardInfo
from lvfs.tasks.models import Task


def _regenerate_shard_infos(task: Task) -> None:
    # Set ComponentShardInfo in ComponentShard if GUID matches
    infos: Dict[str, ComponentShardInfo] = {}
    for info in db.session.query(ComponentShardInfo):
        if info.guid in infos:
            # the GUID already exists; remove the old ComponentShardInfo
            oldinfo = infos[info.guid]
            for shard in oldinfo.shards:
                task.add_pass(
                    f"Reparenting shard {shard.component_shard_id} with {shard.guid}"
                )
                shard.component_shard_info_id = info.component_shard_info_id
            if oldinfo.claim_id:
                info.claim_id = oldinfo.claim_id
            db.session.delete(oldinfo)
            db.session.commit()
        infos[info.guid] = info

    task.percentage_current = 0
    task.percentage_total = db.session.query(ComponentShard).count()
    cnt_added: int = 0
    cnt_fixed: int = 0
    for (component_shard_id,) in db.session.query(
        ComponentShard.component_shard_id
    ).filter(ComponentShard.component_shard_info_id == None):
        task.percentage_current += 1
        task.status = f"ComponentShard {component_shard_id}"
        shard = (
            db.session.query(ComponentShard)
            .filter(ComponentShard.component_shard_id == component_shard_id)
            .with_for_update(of=ComponentShard)
            .first()
        )
        if not shard:
            continue
        shard.info = infos.get(shard.guid)
        if shard.info:
            cnt_fixed += 1
        else:
            info = ComponentShardInfo(guid=shard.guid)
            db.session.add(info)
            shard.info = info
            infos[shard.guid] = shard.info
            cnt_added += 1
        if task.percentage_current % 50 == 0:
            db.session.commit()
    if cnt_added:
        task.add_pass(f"Created {cnt_added} ComponentShardInfo")
    if cnt_fixed:
        task.add_pass(f"Fixed {cnt_fixed} ComponentShards")

    # update ComponentShardInfo.cnt
    task.percentage_current = 0
    task.percentage_total = db.session.query(ComponentShardInfo).count()
    for (info_id,) in db.session.query(
        ComponentShardInfo.component_shard_info_id
    ).order_by(ComponentShardInfo.component_shard_info_id.asc()):
        task.percentage_current += 1
        task.status = f"ComponentShardInfo {info_id}"
        info = (
            db.session.query(ComponentShardInfo)
            .filter(ComponentShardInfo.component_shard_info_id == info_id)
            .with_for_update(of=ComponentShardInfo)
            .one()
        )
        info.cnt = (
            db.session.query(ComponentShard.component_shard_id)
            .filter(ComponentShard.guid == info.guid)
            .count()
        )
        if task.percentage_current % 50 == 0:
            db.session.commit()

    # work out most likely name
    task.status = "ComponentShardInfo names..."
    task.percentage_current = 0
    db.session.commit()
    for info in (
        db.session.query(ComponentShardInfo)
        .filter(ComponentShardInfo.cnt > 0)
        .filter(
            (ComponentShardInfo.name == None)
            | (ComponentShardInfo.name.startswith("org.uefi.Unknown"))
        )
    ):
        task.percentage_current += 1
        task.status = f"ComponentShardInfo {info.component_shard_info_id} name"
        cnt: Counter = Counter()
        for shard in info.shards:
            if not shard.name:
                continue
            if shard.name in [
                "com.intel.Microcode",
                "org.uefi",
                "org.uefi.Application",
                "org.uefi.Driver",
                "org.uefi.Dxe",
                "org.uefi.NVRAM",
                "org.uefi.Padding",
                "org.uefi.Pei",
                "org.uefi.Peim",
                "org.uefi.Raw",
                "org.uefi.Security",
                "org.uefi.SmmDxe",
            ]:
                continue
            cnt[shard.name] += 1
        for name, _ in cnt.most_common():
            # ignore the HP hex versions
            try:
                _ = int(name.rsplit(".")[-1], 16)
                continue
            except ValueError:
                pass
            info.name = name
            task.add_pass(f"Fixing shard {info.guid} to have name {name}")
            break
        if task.percentage_current % 50 == 0:
            db.session.commit()
    db.session.commit()


def task_regenerate(task: Task) -> None:
    _regenerate_shard_infos(task)
