#!/usr/bin/python3
#
# Copyright 2019 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later

import json
from typing import Any

from flask import Blueprint, url_for, redirect, flash, render_template, g
from flask_login import login_required
from sqlalchemy.orm import joinedload
from sqlalchemy.exc import NoResultFound

from lvfs import db

from lvfs.tests.models import Test, TestResult
from lvfs.util import admin_login_required
from lvfs.tasks.models import Task

bp_tests = Blueprint("tests", __name__, template_folder="templates")


@bp_tests.route("/recent")
@login_required
@admin_login_required
def route_recent() -> Any:
    tests = (
        db.session.query(Test)
        .filter(Test.started_ts != None)
        .filter(Test.ended_ts != None)
        .order_by(Test.ended_ts.desc())
        .limit(20)
        .options(joinedload(Test.attributes), joinedload(Test.fw))
        .all()
    )
    return render_template("test-list.html", category="tests", tests=tests)


@bp_tests.route("/running")
@login_required
@admin_login_required
def route_running() -> Any:
    tests = (
        db.session.query(Test)
        .filter(Test.started_ts != None)
        .filter(Test.ended_ts == None)
        .order_by(Test.scheduled_ts.desc())
        .limit(100)
        .options(joinedload(Test.attributes), joinedload(Test.fw))
        .all()
    )
    return render_template("test-list.html", category="tests", tests=tests)


@bp_tests.route("/pending")
@login_required
@admin_login_required
def route_pending() -> Any:
    tests = (
        db.session.query(Test)
        .filter(Test.started_ts == None)
        .order_by(Test.scheduled_ts.desc())
        .limit(100)
        .options(joinedload(Test.attributes), joinedload(Test.fw))
        .all()
    )
    return render_template("test-list.html", category="tests", tests=tests)


@bp_tests.route("/failed")
@login_required
@admin_login_required
def route_failed() -> Any:
    tests = (
        db.session.query(Test)
        .filter(Test.ended_ts != None)
        .filter(Test.waived_ts == None)
        .order_by(Test.scheduled_ts.desc())
        .limit(100)
        .options(joinedload(Test.attributes), joinedload(Test.fw))
        .all()
    )
    tests_failed: list[Test] = []
    for test in tests:
        if test.result != TestResult.PASS:
            tests_failed.append(test)
    return render_template("test-list.html", category="tests", tests=tests_failed)


@bp_tests.route("/waived")
@login_required
@admin_login_required
def route_waived() -> Any:
    tests = (
        db.session.query(Test)
        .filter(Test.ended_ts != None)
        .filter(Test.waived_ts != None)
        .order_by(Test.scheduled_ts.desc())
        .limit(100)
        .options(joinedload(Test.attributes), joinedload(Test.fw))
        .all()
    )
    return render_template("test-list.html", category="tests", tests=tests)


@bp_tests.post("/retry/<int:test_id>")
@login_required
def route_retry(test_id: int) -> Any:
    # get test
    try:
        test = (
            db.session.query(Test)
            .filter(Test.test_id == test_id)
            .with_for_update(of=Test)
            .one()
        )
    except NoResultFound:
        flash("No test matched", "danger")
        return redirect(url_for("main.route_dashboard"))

    # security check
    if not test.check_acl("@retry"):
        flash("Permission denied: Unable to retry test", "danger")
        return redirect(
            url_for("firmware.route_tests", firmware_id=test.fw.firmware_id)
        )

    # remove child
    test.retry()
    db.session.commit()

    # asynchronously run
    db.session.add(
        Task(
            value=json.dumps({"id": test.test_id}),
            caller=__name__,
            user=g.user,
            url=url_for("firmware.route_tests", firmware_id=test.fw.firmware_id),
            function="lvfs.tests.utils.task_test_run",
        )
    )
    db.session.commit()

    # log
    flash(f"Test {test.plugin_id} will be re-run soon", "info")
    return redirect(url_for("firmware.route_tests", firmware_id=test.fw.firmware_id))


@bp_tests.post("/ensure")
@login_required
@admin_login_required
def route_ensure() -> Any:
    # asynchronously run
    db.session.add(
        Task(
            caller=__name__,
            user=g.user,
            function="lvfs.tests.utils.task_test_ensure",
        )
    )
    db.session.commit()

    flash("All firmware will be scanned and new tests added soon", "info")
    return redirect(url_for("main.route_dashboard"))


@bp_tests.post("/waive/<int:test_id>")
@login_required
def route_waive(test_id: int) -> Any:
    # get test
    try:
        test = (
            db.session.query(Test)
            .filter(Test.test_id == test_id)
            .with_for_update(of=Test)
            .one()
        )
    except NoResultFound:
        flash("No test matched", "danger")
        return redirect(url_for("main.route_dashboard"))

    # security check
    if not test.waivable or not test.check_acl("@waive"):
        flash("Permission denied: Unable to waive test", "danger")
        return redirect(
            url_for("firmware.route_tests", firmware_id=test.fw.firmware_id)
        )

    # remove chid
    test.waive()
    db.session.commit()

    # log
    flash(f"Test {test.plugin_id} was waived", "info")
    return redirect(url_for("firmware.route_tests", firmware_id=test.fw.firmware_id))


@bp_tests.post("/unwaive/<int:test_id>")
@login_required
@admin_login_required
def route_unwaive(test_id: int) -> Any:
    # get test
    try:
        test = (
            db.session.query(Test)
            .filter(Test.test_id == test_id)
            .with_for_update(of=Test)
            .one()
        )
    except NoResultFound:
        flash("No test matched", "danger")
        return redirect(url_for("main.route_dashboard"))
    test.unwaive()
    db.session.commit()
    flash(f"Test {test.plugin_id} was unwaived", "info")
    return redirect(url_for("firmware.route_tests", firmware_id=test.fw.firmware_id))


@bp_tests.post("/retry/<plugin_id>")
@login_required
@admin_login_required
def route_retry_all(plugin_id: str) -> Any:
    # get tests
    tests = (
        db.session.query(Test)
        .filter(Test.started_ts != None)
        .filter(Test.plugin_id == plugin_id)
        .order_by(Test.scheduled_ts.desc())
        .with_for_update(of=Test)
        .all()
    )
    if not tests:
        flash("No tests matched", "warning")
        return redirect(url_for("settings.route_view", plugin_id=plugin_id))

    # asynchronously run
    for test in tests:
        test.retry()
        db.session.add(
            Task(
                value=json.dumps({"id": test.test_id}),
                caller=__name__,
                user=g.user,
                function="lvfs.tests.utils.task_test_run",
            )
        )
    db.session.commit()

    # log
    flash(f"{len(tests)} tests will be re-run soon", "info")
    return redirect(url_for("settings.route_view", plugin_id=plugin_id))


@bp_tests.post("/waive/<plugin_id>")
@login_required
@admin_login_required
def route_waive_all(plugin_id: str) -> Any:
    # get tests
    tests = (
        db.session.query(Test)
        .filter(Test.ended_ts != None)
        .filter(Test.plugin_id == plugin_id)
        .order_by(Test.scheduled_ts.desc())
        .with_for_update(of=Test)
        .all()
    )
    tests_failed: list[Test] = []
    for test in tests:
        if test.result != TestResult.PASS:
            tests_failed.append(test)
    if not tests_failed:
        flash("No tests could be waived", "warning")
        return redirect(url_for("settings.route_view", plugin_id=plugin_id))
    for test in tests_failed:
        test.waive()
    db.session.commit()

    # log
    flash(f"{len(tests_failed)} tests have been waived", "info")
    return redirect(url_for("settings.route_view", plugin_id=plugin_id))


@bp_tests.post("/delete/<plugin_id>")
@login_required
@admin_login_required
def route_delete_all(plugin_id: str) -> Any:
    # get tests
    tests = (
        db.session.query(Test)
        .filter(Test.plugin_id == plugin_id)
        .with_for_update(of=Test)
        .all()
    )
    if not tests:
        flash("No tests matched", "warning")
        return redirect(url_for("settings.route_view", plugin_id=plugin_id))
    for test in tests:
        db.session.delete(test)
    db.session.commit()

    # log
    flash(f"{len(tests)} tests have been deleted", "info")
    return redirect(url_for("settings.route_view", plugin_id=plugin_id))
