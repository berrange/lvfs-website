#!/usr/bin/python3
#
# Copyright 2022 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later

from lvfs.pluginloader import PluginBase


class Plugin(PluginBase):
    def __init__(self) -> None:
        PluginBase.__init__(self, "system_integrity")
        self.name = "System Integrity"
        self.summary = "Verify system integrity using client report data"
