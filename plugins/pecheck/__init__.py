#!/usr/bin/python3
#
# Copyright 2019 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later
#
# pylint: disable=no-member,too-few-public-methods,protected-access

import os
import datetime
from typing import Any, Optional

from pyasn1.codec.der import decoder as der_decoder
from pyasn1.error import PyAsn1Error
from pyasn1_modules import rfc2315
from pyasn1_modules import rfc2459

import pefile

from lvfs import db
from lvfs.pluginloader import (
    PluginBase,
    PluginSettingBool,
    PluginSettingInt,
)
from lvfs.tests.models import Test
from lvfs.components.models import Component, ComponentShardCertificate, ComponentShard
from lvfs.firmware.models import Firmware


def _build_rfc2459_description(value: Any) -> str:
    descs: list[str] = []
    for val in value:
        attr_type = val[0]["type"]
        attr_blob: bytes = bytes(val[0]["value"])[2:]  # prefixed with uint16 length
        if not attr_blob:
            continue

        # some Lenovo certs encode this as UTF-16BE!
        if attr_blob[0] == 0x0:
            attr_value = attr_blob.decode("utf-16be")
        else:
            attr_value = attr_blob.decode()

        if attr_type == rfc2459.id_at_countryName:
            descs.append(f"C={attr_value}")
        elif attr_type == rfc2459.id_at_dnQualifier:
            descs.append(f"DN={attr_value}")
        elif attr_type == rfc2459.id_at_organizationalUnitName:
            descs.append(f"OU={attr_value}")
        elif attr_type == rfc2459.id_at_organizationName:
            descs.append(f"O={attr_value}")
        elif attr_type == rfc2459.id_at_stateOrProvinceName:
            descs.append(f"ST={attr_value}")
        elif attr_type == rfc2459.id_at_localityName:
            descs.append(f"L={attr_value}")
        elif attr_type == rfc2459.id_at_commonName:
            descs.append(f"CN={attr_value}")
        elif attr_type == rfc2459.id_at_givenName:
            descs.append(f"GN={attr_value}")
        elif attr_type == rfc2459.id_at_surname:
            descs.append(f"SN={attr_value}")
        elif attr_type == rfc2459.id_at_name:
            descs.append(f"N={attr_value}")
    return ", ".join(descs)


def _extract_authenticode_tbscerts(tbscert: Any) -> ComponentShardCertificate:
    cert = ComponentShardCertificate(kind="Authenticode PKCS7")
    cert.serial_number = str(tbscert["serialNumber"])
    if "validity" in tbscert:
        validity = tbscert["validity"]
        cert.not_before = validity["notBefore"]["utcTime"].asDateTime.replace(
            tzinfo=None
        )
        cert.not_after = validity["notAfter"]["utcTime"].asDateTime.replace(tzinfo=None)
    issuer = tbscert["issuer"]
    for value in issuer.values():
        cert.description = _build_rfc2459_description(value)
        break
    return cert


def _extract_certs_from_authenticode_blob(
    buf: bytes,
) -> list[ComponentShardCertificate]:
    try:
        contentInfo, _ = der_decoder.decode(buf, asn1Spec=rfc2315.ContentInfo())
    except PyAsn1Error:
        return []
    contentType = contentInfo.getComponentByName("contentType")
    contentInfoMap = {
        (1, 2, 840, 113549, 1, 7, 1): rfc2315.Data(),
        (1, 2, 840, 113549, 1, 7, 2): rfc2315.SignedData(),
        (1, 2, 840, 113549, 1, 7, 3): rfc2315.EnvelopedData(),
        (1, 2, 840, 113549, 1, 7, 4): rfc2315.SignedAndEnvelopedData(),
        (1, 2, 840, 113549, 1, 7, 5): rfc2315.DigestedData(),
        (1, 2, 840, 113549, 1, 7, 6): rfc2315.EncryptedData(),
    }
    content, _ = der_decoder.decode(
        contentInfo.getComponentByName("content"), asn1Spec=contentInfoMap[contentType]
    )
    certs: list[ComponentShardCertificate] = []
    for cert in content["certificates"]:
        tbscert = cert["certificate"]["tbsCertificate"]
        certs.append(_extract_authenticode_tbscerts(tbscert))
    for c in content["signerInfos"]:
        tbscert = c["issuerAndSerialNumber"]
        certs.append(_extract_authenticode_tbscerts(tbscert))
    return certs


class Plugin(PluginBase):
    def __init__(self) -> None:
        PluginBase.__init__(self, "pecheck")
        self.name = "PE Check"
        self.summary = "Check the portable executable file (.efi) for common problems"
        self.order_after = ["uefi-extract", "intelme"]
        self.settings.append(
            PluginSettingBool(key="pecheck_enabled", name="Enabled", default=True)
        )
        self.settings.append(
            PluginSettingInt(
                key="pecheck_allowable",
                name="Number of years to relax failure",
                default=3,
            )
        )

    def require_test_for_md(self, md: Component) -> bool:
        if not md.protocol:
            return False
        return md.protocol.value == "org.uefi.capsule"

    def ensure_test_for_fw(self, fw: Firmware) -> None:
        # add if not already exists
        test = fw.find_test_by_plugin_id(self.id)
        if not test:
            test = Test(plugin_id=self.id, waivable=True)
            fw.tests.append(test)

    def _run_test_on_shard(self, test: Test, shard: ComponentShard) -> None:

        blob: Optional[bytes] = shard.read()
        if not blob:
            return
        try:
            # parse PE header
            pe = pefile.PE(data=blob)

            # get optional directory entry
            security = pe.OPTIONAL_HEADER.DATA_DIRECTORY[
                pefile.DIRECTORY_ENTRY["IMAGE_DIRECTORY_ENTRY_SECURITY"]
            ]
            if security.VirtualAddress == 0 or security.Size == 0:
                return

            # format as a blob
            signature = pe.write()[
                security.VirtualAddress + 8 : security.VirtualAddress + security.Size
            ]
            if not signature:
                return
            if len(signature) != security.Size - 8:
                test.add_fail(
                    shard.name,
                    "Unable to extract full signature, file is most likely truncated -- "
                    f"Extracted: {len(signature)} bytes, "
                    f"expected: {security.Size - 8} bytes",
                )
                return
        except pefile.PEFormatError:
            # not a PE file, which is fine
            return

        # get all the certificates and signer
        certs = _extract_certs_from_authenticode_blob(signature)
        if not certs:
            return

        # check the certs are valid, relaxing the notAfter checks by a good margin
        dtallowable = datetime.timedelta(
            days=self.get_setting_int("pecheck_allowable") * 365
        )
        for cert in certs:
            if cert.not_before and cert.not_before > shard.md.fw.timestamp.replace(
                tzinfo=None
            ):
                test.add_warn(
                    shard.name,
                    f"Authenticode certificate invalid before {cert.not_before}: {cert.description}",
                )
            elif (
                cert.not_after
                and cert.not_after
                < shard.md.fw.timestamp.replace(tzinfo=None) - dtallowable
            ):
                test.add_warn(
                    shard.name,
                    f"Authenticode certificate invalid after {cert.not_after}: {cert.description}",
                )

        # save certificate
        for cert in certs:
            cert.plugin_id = self.id
            cert.component_shard_id = shard.component_shard_id
            shard.certificates.append(cert)

    def run_test_on_md(self, test: Test, md: Component) -> None:
        # run analysis on each shard
        for cert in md.certificates:
            if cert.plugin_id == self.id:
                db.session.delete(cert)
        for shard in md.shards:
            self._run_test_on_shard(test, shard)


# run with PYTHONPATH=. ./env/bin/python3 plugins/pecheck/__init__.py
if __name__ == "__main__":
    import sys
    import zlib

    from lvfs import create_app
    from lvfs.protocols.models import Protocol

    with create_app().app_context():
        for _argv in sys.argv[1:]:
            print("Processing", _argv)
            plugin = Plugin()
            _test = Test(plugin_id=plugin.id)
            _fw = Firmware()
            _fw.timestamp = datetime.datetime.utcnow()
            _md = Component(fw=_fw)
            _md.protocol = Protocol(value="org.uefi.capsule")
            _shard = ComponentShard(component_id=999, name=os.path.basename(_argv))
            try:
                with open(_argv, "rb") as f:
                    _shard.write(zlib.decompressobj().decompress(f.read()))
            except zlib.error:
                with open(_argv, "rb") as f:
                    _shard.write(f.read())
            except IsADirectoryError:
                continue
            _md.shards.append(_shard)
            plugin.run_test_on_md(_test, _md)
            for attribute in _test.attributes:
                print(attribute)
            for _cert in _shard.certificates:
                print(_cert)
