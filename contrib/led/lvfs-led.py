#!/usr/bin/python3
#
# Copyright 2020 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later

import sys
import json
import time

import requests
import serial

LED_REFRESH_DELAY = 100  # ms
LVFS_REFRESH_DELAY = 5000  # ms


def _led_update(dev: str) -> int:
    # no log in required
    session = requests.Session()
    try:
        ser = serial.Serial(dev, 38400)
        ser.write_timeout = 5
    except serial.serialutil.SerialException as e:
        print(f"failed to write: {e}")
        return 2

    idx = 0
    cnt_last = 0
    cnt_new = 0
    fwt_cnt = 0
    fws_cnt = 0
    while True:
        # hit the public endpoint
        if idx == 0:
            try:
                rv = session.get("https://fwupd.org/lvfs/metrics", timeout=5)
            except (
                requests.exceptions.ConnectionError,
                requests.exceptions.ReadTimeout,
            ) as e:
                print(str(e))
                cnt_new = 0
                display = "Conn Err"
            else:
                if rv.status_code != 200:
                    cnt_new = 0
                    display = f"StAtuS {rv.status_code}"
                else:
                    # parse JSON data
                    try:
                        item = json.loads(rv.content.decode())
                    except ValueError as e:
                        print(f"No JSON object could be decoded: {e!s}")
                        cnt_new = 0
                        display = "JSOn bAd"
                    else:
                        cnt_last = cnt_new
                        cnt_new = int(item["ClientCnt"])
                        fwt_cnt = int(item["FirmwareCnt"])
                        fws_cnt = int(item["FirmwareStableCnt"])

        # linearly interpolate between the old and new values
        if cnt_new and cnt_last:
            cnt_diff = cnt_new - cnt_last
            pc = (LED_REFRESH_DELAY / LVFS_REFRESH_DELAY) * idx
            display = f"{int(cnt_last + (cnt_diff * pc)): >12}"

        # we only have the initial value
        elif cnt_new:
            display = f"{cnt_new: >12}"

        # firmware in each state
        if fws_cnt and fwt_cnt:
            display += f"{fws_cnt: >6}{fwt_cnt: >6}"

        # send to serial port
        try:
            display += "\n"
            ser.write(display.encode())
        except serial.serialutil.SerialException as e:
            print(f"failed to write: {e}")
            return 1

        # wait a bit
        time.sleep(LED_REFRESH_DELAY / 1000)

        # increment the counter, resetting if it gets high enough
        idx += 1
        if idx == LVFS_REFRESH_DELAY / LED_REFRESH_DELAY:
            idx = 0


if __name__ == "__main__":
    try:
        _dev = f"/dev/{sys.argv[1]}"
    except IndexError:
        _dev = "/dev/ttyUSB0"
    _led_update(_dev)
