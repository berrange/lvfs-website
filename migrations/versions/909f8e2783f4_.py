"""empty message

Revision ID: 909f8e2783f4
Revises: 9995714a32cf
Create Date: 2024-04-21 16:00:40.280360

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "909f8e2783f4"
down_revision = "9995714a32cf"
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "component_licenses",
        sa.Column("component_license_id", sa.Integer(), nullable=False),
        sa.Column("component_id", sa.Integer(), nullable=False),
        sa.Column("license_id", sa.Integer(), nullable=True),
        sa.Column("user_id", sa.Integer(), nullable=False),
        sa.Column("ctime", sa.DateTime(), nullable=False),
        sa.ForeignKeyConstraint(
            ["component_id"],
            ["components.component_id"],
        ),
        sa.ForeignKeyConstraint(
            ["license_id"],
            ["licenses.license_id"],
        ),
        sa.ForeignKeyConstraint(
            ["user_id"],
            ["users.user_id"],
        ),
        sa.PrimaryKeyConstraint("component_license_id"),
        sa.UniqueConstraint(
            "component_id",
            "license_id",
            name="uq_component_licenses_component_id_license_id",
        ),
    )
    with op.batch_alter_table("component_licenses", schema=None) as batch_op:
        batch_op.create_index(
            batch_op.f("ix_component_licenses_component_id"),
            ["component_id"],
            unique=False,
        )


def downgrade():
    with op.batch_alter_table("component_licenses", schema=None) as batch_op:
        batch_op.drop_index(batch_op.f("ix_component_licenses_component_id"))
    op.drop_table("component_licenses")
