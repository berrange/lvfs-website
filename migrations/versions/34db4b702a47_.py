"""empty message

Revision ID: 34db4b702a47
Revises: 4eb6cb484b9c
Create Date: 2023-05-18 16:11:23.631654

"""
from alembic import op
import sqlalchemy as sa


revision = "34db4b702a47"
down_revision = "4eb6cb484b9c"
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "mirrors",
        sa.Column("mirror_id", sa.Integer(), nullable=False),
        sa.Column("remote_id", sa.Integer(), nullable=True),
        sa.Column("task_id", sa.Integer(), nullable=True),
        sa.Column("size_now", sa.BigInteger(), nullable=True),
        sa.Column("size_total", sa.BigInteger(), nullable=True),
        sa.Column("name", sa.Text(), nullable=False),
        sa.Column("enabled", sa.Boolean(), nullable=True),
        sa.Column("remove_deleted", sa.Boolean(), nullable=True),
        sa.Column("uri", sa.Text(), nullable=True),
        sa.Column("auth_username", sa.Text(), nullable=True),
        sa.Column("auth_token", sa.Text(), nullable=True),
        sa.Column("user_id", sa.Integer(), nullable=False),
        sa.Column("ctime", sa.DateTime(), nullable=False),
        sa.Column("comment", sa.Text(), nullable=True),
        sa.ForeignKeyConstraint(
            ["remote_id"],
            ["remotes.remote_id"],
        ),
        sa.ForeignKeyConstraint(
            ["task_id"],
            ["tasks.task_id"],
        ),
        sa.ForeignKeyConstraint(
            ["user_id"],
            ["users.user_id"],
        ),
        sa.PrimaryKeyConstraint("mirror_id"),
        sa.UniqueConstraint("uri"),
    )
    with op.batch_alter_table("mirrors", schema=None) as batch_op:
        batch_op.create_index(batch_op.f("ix_mirrors_name"), ["name"], unique=True)

    op.create_table(
        "mirror_checksums",
        sa.Column("mirror_checksum_id", sa.Integer(), nullable=False),
        sa.Column("mirror_id", sa.Integer(), nullable=False),
        sa.Column("valid", sa.Boolean(), nullable=True),
        sa.Column("basename", sa.Text(), nullable=False),
        sa.Column("checksum", sa.Text(), nullable=False),
        sa.Column("size", sa.Integer(), nullable=False),
        sa.Column("ctime", sa.DateTime(), nullable=False),
        sa.ForeignKeyConstraint(
            ["mirror_id"],
            ["mirrors.mirror_id"],
        ),
        sa.PrimaryKeyConstraint("mirror_checksum_id"),
    )
    with op.batch_alter_table("mirror_checksums", schema=None) as batch_op:
        batch_op.create_index(
            batch_op.f("ix_mirror_checksums_basename"), ["basename"], unique=False
        )
        batch_op.create_index(
            batch_op.f("ix_mirror_checksums_checksum"), ["checksum"], unique=False
        )
        batch_op.create_index(
            batch_op.f("ix_mirror_checksums_mirror_id"), ["mirror_id"], unique=False
        )

    op.create_table(
        "mirror_filters",
        sa.Column("mirror_tag_id", sa.Integer(), nullable=False),
        sa.Column("mirror_id", sa.Integer(), nullable=False),
        sa.Column("key", sa.Text(), nullable=False),
        sa.Column("value", sa.Text(), nullable=False),
        sa.ForeignKeyConstraint(
            ["mirror_id"],
            ["mirrors.mirror_id"],
        ),
        sa.PrimaryKeyConstraint("mirror_tag_id"),
    )
    with op.batch_alter_table("mirror_filters", schema=None) as batch_op:
        batch_op.create_index(
            batch_op.f("ix_mirror_filters_mirror_id"), ["mirror_id"], unique=False
        )


def downgrade():
    with op.batch_alter_table("mirror_filters", schema=None) as batch_op:
        batch_op.drop_index(batch_op.f("ix_mirror_filters_mirror_id"))

    op.drop_table("mirror_filters")
    with op.batch_alter_table("mirror_checksums", schema=None) as batch_op:
        batch_op.drop_index(batch_op.f("ix_mirror_checksums_mirror_id"))
        batch_op.drop_index(batch_op.f("ix_mirror_checksums_checksum"))
        batch_op.drop_index(batch_op.f("ix_mirror_checksums_basename"))

    op.drop_table("mirror_checksums")
    with op.batch_alter_table("mirrors", schema=None) as batch_op:
        batch_op.drop_index(batch_op.f("ix_mirrors_name"))

    op.drop_table("mirrors")
