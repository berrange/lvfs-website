"""empty message

Revision ID: 3d7c893bbd0b
Revises: 2db5b7fce2c7
Create Date: 2023-07-06 13:47:39.949905

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "3d7c893bbd0b"
down_revision = "2db5b7fce2c7"
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("remotes", schema=None) as batch_op:
        batch_op.add_column(sa.Column("is_valid", sa.Boolean(), nullable=True))
    with op.batch_alter_table("firmware", schema=None) as batch_op:
        batch_op.add_column(sa.Column("is_metadata_valid", sa.Boolean(), nullable=True))


def downgrade():
    with op.batch_alter_table("remotes", schema=None) as batch_op:
        batch_op.drop_column("is_valid")
    with op.batch_alter_table("firmware", schema=None) as batch_op:
        batch_op.drop_column("is_metadata_valid")
