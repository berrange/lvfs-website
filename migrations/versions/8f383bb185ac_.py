"""empty message

Revision ID: 8f383bb185ac
Revises: 7fc43b731948
Create Date: 2023-08-31 10:59:09.575030

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "8f383bb185ac"
down_revision = "7fc43b731948"
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "analytics_claim",
        sa.Column("analytic_id", sa.Integer(), nullable=False),
        sa.Column("datestr", sa.Integer(), nullable=True),
        sa.Column("claim_id", sa.Integer(), nullable=False),
        sa.Column("cnt", sa.Integer(), nullable=True),
        sa.ForeignKeyConstraint(
            ["claim_id"],
            ["claims.claim_id"],
        ),
        sa.PrimaryKeyConstraint("analytic_id"),
    )
    with op.batch_alter_table("analytics_claim", schema=None) as batch_op:
        batch_op.create_index(
            batch_op.f("ix_analytics_claim_claim_id"), ["claim_id"], unique=False
        )
        batch_op.create_index(
            batch_op.f("ix_analytics_claim_datestr"), ["datestr"], unique=False
        )


def downgrade():
    with op.batch_alter_table("analytics_claim", schema=None) as batch_op:
        batch_op.drop_index(batch_op.f("ix_analytics_claim_datestr"))
        batch_op.drop_index(batch_op.f("ix_analytics_claim_claim_id"))
    op.drop_table("analytics_claim")
