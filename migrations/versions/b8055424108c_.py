"""empty message

Revision ID: b8055424108c
Revises: 52850db6d284
Create Date: 2024-05-08 09:52:02.271058

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "b8055424108c"
down_revision = "52850db6d284"
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("guids", schema=None) as batch_op:
        batch_op.add_column(sa.Column("user_id", sa.Integer(), nullable=True))
        batch_op.create_foreign_key(None, "users", ["user_id"], ["user_id"])


def downgrade():
    with op.batch_alter_table("guids", schema=None) as batch_op:
        batch_op.drop_constraint(None, type_="foreignkey")
        batch_op.drop_column("user_id")
