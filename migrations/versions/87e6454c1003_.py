"""empty message

Revision ID: 87e6454c1003
Revises: 34db4b702a47
Create Date: 2023-05-19 13:44:12.573024

"""
from alembic import op
import sqlalchemy as sa


revision = "87e6454c1003"
down_revision = "34db4b702a47"
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("components", schema=None) as batch_op:
        batch_op.add_column(sa.Column("developer_name", sa.Text(), nullable=True))


def downgrade():
    with op.batch_alter_table("components", schema=None) as batch_op:
        batch_op.drop_column("developer_name")
