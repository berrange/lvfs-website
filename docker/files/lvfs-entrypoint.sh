#!/bin/bash
set -e

function ecs_metadata {
    if [ -n "$ECS_CONTAINER_METADATA_URI_V4" ]; then
        curl ${ECS_CONTAINER_METADATA_URI_V4} --output /tmp/container.json
        export CONTAINER_ID=`jq -r -s '.[0].DockerId' /tmp/container.json`
    else
        export CONTAINER_ID="unset"
    fi
    echo "CONTAINER_ID is $CONTAINER_ID"
}

if [ "$DEPLOY" = "application" ]
then
    ecs_metadata
    echo "upgrading database"
    FLASK_APP=lvfs/__init__.py flask db upgrade

    echo "installing splunk-otel-collector"
    export OTEL_TOKEN=`echo $LVFS_CONFIG | jq -r .splunk_otel_token`
    export OTEL_ENV=`echo $LVFS_CONFIG | jq -r .release`
    export OTEL_SERVICE_NAME="lvfs-application"
    export OTEL_RESOURCE_ATTRIBUTES="deployment.environment=${OTEL_ENV},service.version=1.0.0"
    export OTEL_EXPORTER_OTLP_ENDPOINT='http://localhost:4317'
    export SPLUNK_PROFILER_ENABLED="true"

    echo "starting gunicorn"
    exec splunk-py-trace gunicorn --config /app/conf/gunicorn.py "wsgi:app"
fi

if [ "$DEPLOY" = "metadata" ]
then
    ecs_metadata
    echo "starting freshclam"
    freshclam # download our files first
    freshclam -v --stdout -d -c 1 --daemon-notify=/etc/clamd.d/scan.conf # then daemonize

    echo "starting clam"
    /usr/sbin/clamd -c /etc/clamd.d/scan.conf

    echo "upgrading database"
    FLASK_APP=lvfs/__init__.py flask db upgrade

    echo "installing splunk-otel-collector"
    export OTEL_TOKEN=`echo $LVFS_CONFIG | jq -r .splunk_otel_token`
    export OTEL_ENV=`echo $LVFS_CONFIG | jq -r .release`
    export OTEL_SERVICE_NAME="lvfs-metadata"
    export OTEL_RESOURCE_ATTRIBUTES="deployment.environment=${OTEL_ENV},service.version=1.0.0"
    export OTEL_EXPORTER_OTLP_ENDPOINT='http://localhost:4317'
    export SPLUNK_PROFILER_ENABLED="true"

    echo "starting worker"
    PYTHONPATH=. exec splunk-py-trace python lvfs/worker.py
fi

if [ "$DEPLOY" = "test" ]
then
    source /app/venv/bin/activate
    pip install mypy
fi
