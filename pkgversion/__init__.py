#!/usr/bin/python3
#
# Copyright 2018 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later

from .vercmp import vercmp

__all__ = ["vercmp"]
